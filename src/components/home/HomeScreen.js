import React, { useEffect, useState } from 'react'
import { Visitor } from '../../models/visitor';
import { _post } from '../../services/petcionesHttp';
import './HomeScreen.css'
import Button from '@material-ui/core/Button';

export const HomeScreen = ({ history }) => {

    const [visitor, setVisitor] = useState(new Visitor());

    useEffect(
        () => {
            _post(process.env.REACT_APP_URL_VISITOR)
            .then(({data}) =>{
               setVisitor(data.data);
            })
            .catch(() => {
                console.error("No se logró establecer conexión con el servicio de visitantes.");
            });
        },[] 
    );

    const handleEcommerce = () => {
        history.replace('/e-commerce');
    }

    return (
        <div className="home-container">
            <div className="wrap-candidato">
                <img className="images" src="http://www.grupoasesores.com.mx/img/logo.png" className="mb-2" alt="Logo Gapsi"  /> 
                <br />  
                <img className="images" src="https://scontent.fmex26-1.fna.fbcdn.net/v/t1.0-1/p160x160/103617753_2421600294609775_9084847350996394035_n.jpg?_nc_cat=105&ccb=3&_nc_sid=dbb9e7&_nc_ohc=QgKtSRAlcMYAX_WzC71&_nc_ht=scontent.fmex26-1.fna&tp=6&oh=ca3455e23d6932d72f20843c02ed7720&oe=604D5FFA" className="mb-2" alt="Logo Gapsi"  />
                <h3>{visitor.welcome}</h3>
                <hr />
                <p>{visitor.version}</p>
                <Button
                   variant="contained" 
                   color="primary"
                    onClick={ handleEcommerce }
                >
                    Continuar
                </Button>
            </div> 
        </div>
    )
}
