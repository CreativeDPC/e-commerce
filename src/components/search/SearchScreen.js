import React, { useState } from 'react'
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import './SearchScreen.css'
import { name } from "faker";
import { ProductList } from '../products/ProductList';
import { _get } from '../../services/petcionesHttp';


export const SearchScreen = ({ history }) => {
    const [search, setSearch] = useState("");
    const [pages, setPages] = useState({
        hasNextPage: true,
        isNextPageLoading: true,
        items: []
    });

    const _loadNextPage = (page) => {
        setPages({ ...pages, isNextPageLoading: true });
        
        const url = `https://node-red-nxdup.mybluemix.net/productos/${search}/${(page/10)+1}`; 
        console.log(url);
        _get(url)
        .then(({data}) =>{
            if(data.data){
                setPages({
                    hasNextPage: pages.items.length < 1000,
                    isNextPageLoading: false,
                    items: [...pages.items, ...data.data?.products]
                  });
            }
        })
      };

    const newSearch = () =>{
        setPages({
            hasNextPage: true,
            isNextPageLoading: true,
            items: []
        });
        setTimeout(() => {
            _loadNextPage(0);    
        }, 500);
    }


    return (
        <>
        <div className="container-fluid" style={{height:"100%"}}>
            <div className="row">
                <div className="col-sm-12 col-md-6">
                    <form noValidate autoComplete="off">
                        <InputBase
                            placeholder="Search Google Maps"
                            inputProps={{ 'aria-label': 'Busca por nombre del producto' }}
                            value={search}
                            onChange={(e) => setSearch(e.target.value)}
                        />
                        <IconButton onClick={newSearch}  aria-label="search">
                            <SearchIcon />
                        </IconButton>
                    </form>
                </div>
                <div className="col-sm-12  col-md-6" >
                    <img src={ `./assets/images/carrito-de-compras.png` } className="carrito" alt="carrito" />
                </div>
            </div>
            {
                pages.items.length > 0 &&  
                <div className="row" style={{height:"80%"}}>
                    <div className="col-md-12">
                        <ProductList 
                            hasNextPage={pages.hasNextPage}
                            isNextPageLoading={pages.isNextPageLoading}
                            items={pages.items}
                            loadNextPage={_loadNextPage} 
                        />                    
                    </div>
                </div>
            }
            

        </div>


    </>
    )
}
