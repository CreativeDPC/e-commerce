import React from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MoreVert from '@material-ui/icons/MoreVert';
import './Navbar.css'

export const Navbar = () => {
    return (
      <div className="navbar-container">
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" className="title">
              e-Commerce Gapsi
            </Typography>
            <IconButton edge="start" color="inherit" aria-label="menu">
              <MoreVert />
            </IconButton>
          </Toolbar>
        </AppBar>
      </div>
    );
}