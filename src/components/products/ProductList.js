import React from 'react';
import InfiniteLoader from 'react-window-infinite-loader';
import { FixedSizeList} from "react-window";
import { _get } from '../../services/petcionesHttp';
import AutoSizer from "react-virtualized-auto-sizer";
import { ProductCard } from './ProductCard';

export const ProductList = ({
    hasNextPage,
    isNextPageLoading,
    items,
    loadNextPage
  }) => {

    const itemCount = hasNextPage ? items.length + 1 : items.length;
    const loadMoreItems = isNextPageLoading ? () => {} : loadNextPage;
    const isItemLoaded = index => !hasNextPage || index < items.length;
 
   const Item = ({ index, style }) => {
     let content;
     if (!isItemLoaded(index)) {
        return "Loading...";
     } else {
       return <ProductCard product={items[index]} /> ;
     }
 
     return 
   };
 
   return (
    
     <InfiniteLoader
       isItemLoaded={isItemLoaded}
       itemCount={itemCount}
       loadMoreItems={loadMoreItems}
     >
       {({ onItemsRendered, ref }) => (

            <AutoSizer>
            {({ height, width }) => (
                <FixedSizeList
                className="List"
                height={height}
                itemCount={itemCount}
                itemSize={35}
                onItemsRendered={onItemsRendered}
                ref={ref}
                width={width}
                >
                    {Item}
                </FixedSizeList>
            )}
            </AutoSizer>

       )}
     </InfiniteLoader>
   );
}
