export class Visitor {
    constructor(data = {}){
        this.type = data.type || "";
        this.version = data.version || "";
        this.visitorId = data.visitorId || "";
        this.welcome = data.welcome || "";
    }
} 