import React from 'react'
import { AppRouter } from './routers/App/AppRouter'

export const ECommerceApp = () => {
    return (
        <AppRouter />
    )
}
