import React from 'react';
import ReactDOM from 'react-dom';
import { ECommerceApp } from './ECommerceApp';
//import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <ECommerceApp />
  </React.StrictMode>,
  document.getElementById('root')
);
