
import axios from 'axios';

export const _get=(url)=>{
    return axios.get(url);
}
export const _post = (url, body)=>{
    return axios.post(url,body);
}
