import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from 'react-router-dom';
import { HomeScreen } from '../../components/home/HomeScreen';
import { SearchScreen } from '../../components/search/SearchScreen';
import { Navbar } from '../../components/ui/Navbar';
import './AppRouter.css'
export const AppRouter = () => {
    return (
        <Router>
            <>
                <Navbar/>
                <div className="main-container">
                    <Switch> 
                        <Route exact path="/e-commerce" component={ SearchScreen } />
                        
                        <Route path="/" component={ HomeScreen } />
                    </Switch>
                </div>
            </>
        </Router>
    )
}
